#!/usr/bin/env bash

# ths is needed for some proot specific issue
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/sbin:/sbin

echo adding ulrik pub ssh to pi
mkdir -p /home/pi/.ssh
curl -s https://gitlab.com/ulrik.v.keys > /home/pi/.ssh/authorized_keys
chown pi:pi /home/pi/.ssh -R

chown -vR pi:pi /home/pi/Documents

echo update apt and install stuff
apt-get update
apt-get install -y git python3-pip

echo install python packages for publisher
pip3 install pyserial pyyaml paho-mqtt Adafruit-DHT urllib3 RPi.GPIO

echo "enable ssh at boot time"
touch /boot/ssh

echo 'make serial available (maybe)'
usermod -a -G dialout pi

echo 'ensure that the serial port is not used by linux'
sed -i.bak 's/console=serial0,115200 //' boot/cmdline.txt

echo "enable uart"
echo "enable_uart=1" >> /boot/config.txt
