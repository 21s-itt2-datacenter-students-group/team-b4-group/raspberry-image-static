echo "copying extra files"

MNTDIR="mnt"

echo "Copying network interface to rpi for static IP"
cp templates/ifcfg-interface-eth0 $MNTDIR/etc/network/interfaces.d/eth0

echo "Create Documents folder and copy from this repository"
mkdir $MNTDIR/home/pi/Documents

echo "clone from other project remote mqtt repository"
git clone https://gitlab.com/21s-itt2-datacenter-students-group/team-b4-group/mqtt.git

cp -r -v mqtt $MNTDIR/home/pi/Documents/mqtt
